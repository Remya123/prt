var SalesPersonSearch = function(){
  
  $('#sales_person').autocomplete({
    serviceUrl: '/customer/find_salesperson',
    onSelect: function (suggestion) {
        // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    },
    paramName:'sales_person',
    transformResult: function(response) {
        console.log(response);
        response = JSON.parse(response);
        return {
            suggestions: $.map(response, function(dataItem) {
                console.log(dataItem)
                return { value: dataItem.name, data: dataItem.id };
            })
        };
    }
  })
}