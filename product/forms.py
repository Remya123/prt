from django import forms
from django.forms import ModelForm
from .models import Product


class ProductForm(ModelForm):

    class Meta:
        model = Product
        widgets = {
            "code": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "price": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),

        }
        fields = ['code', 'name', 'price']
