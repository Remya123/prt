from django.conf.urls import include, url

from product import views

app_name = "product"


urlpatterns = [
    url(r'^product/', include([
        url(r'^$', views.index, name="list"),
        url(r'^create/$', views.create, name="create"),
        url(r'^(?P<pk>[^/]+)/edit/$', views.edit, name="edit"),

    ])),

]
