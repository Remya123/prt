from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from django.shortcuts import get_object_or_404, render
from .models import *


@login_required
def create(request):

    productform = ProductForm(request.POST or None)

    if request.method == "POST":

        if productform.is_valid():
            product = productform.save()

            return HttpResponseRedirect(reverse('product:list'))

    context = {
        'productform': productform,
        'form_url': reverse_lazy('product:create')

    }

    return render(request, 'product/create.html', context)


@login_required
def edit(request, pk):
    product = get_object_or_404(Product, pk=pk)
    productform = ProductForm(request.POST or None, instance=product)

    if request.method == "POST":

        if productform.is_valid():
            product = productform.save()

            return HttpResponseRedirect(reverse('product:list'))

    context = {
        'productform': productform,
        'form_url': reverse_lazy('product:edit', args=[product.pk])

    }

    return render(request, 'product/create.html', context)


@login_required
def index(request):

    products = Product.objects.all()

    context = {
        'products': products,

    }

    return render(request, 'product/list.html', context)


@login_required
def detail(request, pk):

    products = get_object_or_404(ProductForm, pk=pk)

    context = {
        'products': products,

    }

    return render(request, 'product/list.html', context)
