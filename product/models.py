from django.conf import settings
from django.db import models
from pms_core.models import ErpAbstractBaseModel
from django.core.validators import MinValueValidator


class Product(ErpAbstractBaseModel):
    code = models.CharField(max_length=255, unique=True, null=True, blank=True)
    name = models.CharField(max_length=255)
    price = models.DecimalField("Selling Price", max_digits=20, decimal_places=2,
                                null=True, blank=True, validators=[MinValueValidator(0)])

    def __str__(self):
        return self.name
