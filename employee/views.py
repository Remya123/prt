from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from django.shortcuts import get_object_or_404, render
from .models import *
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required


@login_required
def create(request):

    userform = UserForm(request.POST or None)
    employeeform = EmployeeForm(request.POST or None)
    addressform = AddressForEmployeeForm(request.POST or None)
    contactform = ContactForEmployeeForm(request.POST or None)

    if request.method == "POST":

        if userform.is_valid() and employeeform.is_valid() and addressform.is_valid() and contactform.is_valid():
            user = userform.save(commit=False)
            user.save()
            group = get_object_or_404(Group, name='employee')
            user.groups.add(group)
            employee = employeeform.save(commit=False)
            employee.user = user
            employee.save()
            address = addressform.save(commit=False)
            address.employe = employee
            address.save()
            contact = contactform.save(commit=False)
            contact.employe = employee
            contact.save()

            return HttpResponseRedirect(reverse('employee:list'))

    context = {
        'userform': userform,
        'customerform': employeeform,
        'addressform': addressform,
        'contactform': contactform,
        'form_url': reverse_lazy('employee:create')

    }

    return render(request, 'employee/create.html', context)


@login_required
def edit(request, pk):
    employee = get_object_or_404(Employee, pk=pk)
    userform = UserChangeForm(request.POST or None, instance=employee.user)
    employeeform = EmployeeForm(request.POST or None, instance=employee)
    addressform = AddressForEmployeeForm(request.POST or None, instance=employee.employee_address)
    contactform = ContactForEmployeeForm(request.POST or None, instance=employee.employee_contact)

    if request.method == "POST":

        if userform.is_valid() and employeeform.is_valid() and addressform.is_valid() and contactform.is_valid():
            user = userform.save()
            employee = employeeform.save(commit=False)
            employee.user = user
            employee.save()
            address = addressform.save(commit=False)
            address.employe = employee
            address.save()
            contact = contactform.save(commit=False)
            contact.employe = employee
            contact.save()

            return HttpResponseRedirect(reverse('employee:list'))

    context = {
        'userform': userform,
        'customerform': employeeform,
        'addressform': addressform,
        'contactform': contactform,
        'form_url': reverse_lazy('employee:edit', args=[employee.pk]),
        "edit": "edit",

    }

    return render(request, 'employee/create.html', context)


@login_required
def index(request):

    employees = Employee.objects.all()

    context = {
        'employees': employees,

    }

    return render(request, 'employee/list.html', context)
