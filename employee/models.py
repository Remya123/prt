from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from address.models import *
from pms_core.models import ErpAbstractBaseModel


class AddressForEmployee(Address):
    employe = models.OneToOneField('Employee', on_delete=models.CASCADE, related_name="employee_address")

    def __str__(self):
        return str(self.employe)


class ContactForEmployee(Contact):
    employe = models.OneToOneField('Employee', on_delete=models.CASCADE, related_name="employee_contact")

    def __str__(self):
        return str(self.employe)


class Employee(ErpAbstractBaseModel):
    SEX = (
        ('Female', 'Female'),
        ('Male', 'Male'),
    )

    MARITALSTATUS = (
        ('Single', 'Single'),
        ('Married', 'Married'),
    )

    BLOODGROUP = (
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('O+', 'O+'),
        ('O-', 'O-'),
    )
    code = models.CharField(max_length=255, unique=True)
    dob = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=8, choices=SEX)
    marital_status = models.CharField(max_length=10, choices=MARITALSTATUS, null=True, blank=True)
    blood_group = models.CharField(max_length=3, choices=BLOODGROUP, null=True, blank=True)
    languages_known = models.CharField(max_length=255, null=True, blank=True)
    religion = models.CharField(max_length=100, null=True, blank=True)
    citizenship = models.CharField(max_length=100)
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)

    def __str__(self):
        return str(self.user.first_name) + " (" + str(self.code) + ")"
