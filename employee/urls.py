from django.conf.urls import include, url

from employee import views

app_name = "employee"


urlpatterns = [
    url(r'^employee/', include([
        url(r'^$', views.index, name="list"),
        url(r'^create/$', views.create, name="create"),
        url(r'^(?P<pk>[^/]+)/edit/$', views.edit, name="edit"),

    ])),

]
