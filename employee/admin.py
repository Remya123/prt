from django.contrib import admin
from .models import *
admin.site.register(Employee)
admin.site.register(AddressForEmployee)
admin.site.register(ContactForEmployee)
