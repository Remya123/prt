from allauth.account.adapter import DefaultAccountAdapter
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse
from django.http import HttpResponse

# from customer.models import Customer


# from django.contrib.auth.models import User

class AccountAdapter(DefaultAccountAdapter):

    def get_login_redirect_url(self, request, success_url=None):

        if success_url is not None:
            return success_url

        if request.user:
            return reverse('/admin/')
        # elif request.user.groups.filter(name__iexact='employee').count() == 1:
        #     return '/employee/'
            # elif request.user.groups.filter(name='canteen_user').count() == 1:
            #     return ''
            # elif request.user.groups.filter(name='Theatre Employee').count() == 1:
            #     return '/theatre'
            # elif request.user.groups.filter(name='Customer').count() == 1:
            #     return reverse('home')
            #     # return reverse('', args=[request.user.username])
        else:
            return reverse('home')
