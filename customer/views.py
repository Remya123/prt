from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from django.shortcuts import get_object_or_404, render
from .models import *


@login_required
def create(request):

    customerform = CustomerForm(request.POST or None)

    if request.method == "POST":

        if customerform.is_valid():
            customer = customerform.save()

            return HttpResponseRedirect(reverse('customer:list'))

    context = {
        'customerform': customerform,
        'form_url': reverse_lazy('customer:create')

    }

    return render(request, 'customer/create.html', context)


@login_required
def edit(request, pk):
    customer = get_object_or_404(Customer, pk=pk)
    customerform = CustomerForm(request.POST or None, instance=customer)

    if request.method == "POST":

        if customerform.is_valid():
            customer = customerform.save()

            return HttpResponseRedirect(reverse('customer:list'))

    context = {
        'customerform': customerform,
        'form_url': reverse_lazy('customer:edit', args=[customer.pk])

    }

    return render(request, 'customer/create.html', context)


@login_required
def index(request):

    customers = Customer.objects.all()

    context = {
        'customers': customers,

    }

    return render(request, 'customer/list.html', context)


@login_required
def detail(request, pk):

    products = get_object_or_404(ProductForm, pk=pk)

    context = {
        'products': products,

    }

    return render(request, 'product/list.html', context)


@login_required
def dashboard(request):

    return render(request, 'pms_temp/dashboard.html')
