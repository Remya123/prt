from django.conf import settings
from django.db import models
from pms_core.models import ErpAbstractBaseModel


class Customer(ErpAbstractBaseModel):
    name = models.CharField(max_length=255)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField("Phone Number", max_length=20)
    alternate_phone = models.CharField("Alternate Phone Number", max_length=20, null=True, blank=True)

    def __str__(self):
        return self.name
