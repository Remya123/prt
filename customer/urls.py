from django.conf.urls import include, url

from customer import views

app_name = "customer"


urlpatterns = [

    url(r'^dashboard/$', views.dashboard, name="dashboard"),
    url(r'^customer/', include([
        url(r'^$', views.index, name="list"),
        url(r'^create/$', views.create, name="create"),
        url(r'^(?P<pk>[^/]+)/edit/$', views.edit, name="edit"),


    ])),

]
