from django import forms
from django.forms import ModelForm
from .models import Customer


class CustomerForm(ModelForm):

    class Meta:
        model = Customer
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "email": forms.EmailInput(attrs={'class': "form-control"}),
            "phone": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "alternate_phone": forms.NumberInput(attrs={'class': "form-control"}),

        }
        fields = ['name', 'email', 'phone', 'alternate_phone']
