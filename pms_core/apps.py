from django.apps import AppConfig


class PmsCoreConfig(AppConfig):
    name = 'pms_core'
