from django.db import models
import uuid


class Country(models.Model):
    country = models.CharField("Country", max_length=200, null=True, blank=False)

    def __str__(self):
        return self.country


class City(models.Model):
    city = models.CharField("City", max_length=200, null=True, blank=False)
    country = models.ForeignKey(Country)

    def __str__(self):
        return self.city


class ErpAbstractBaseModel(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now=True)
