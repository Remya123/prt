from django import forms
from django.forms import ModelForm
from .models import *


class QuotationForm(ModelForm):

    class Meta:
        model = Quotation
        widgets = {
            "customer": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "salesperson": forms.Select(attrs={'class': "form-control"}),
            "shipping_charge": forms.NumberInput(attrs={'class': "form-control"}),


        }
        fields = ['customer', 'salesperson', 'shipping_charge']


class QuotationItemForm(ModelForm):

    class Meta:
        model = QuotationItem
        widgets = {
            "product": forms.Select(attrs={'class': "form-control product", 'required': "required"}),
            "price": forms.NumberInput(attrs={'class': "form-control price", 'required': "required"}),
            "quantity": forms.NumberInput(attrs={'class': "form-control quantity", 'required': "required"}),
            "tax": forms.NumberInput(attrs={'class': "form-control tax"}),
            "total_price": forms.NumberInput(attrs={'class': "form-control total_price"}),
            "total_price_excluding_tax": forms.NumberInput(attrs={'class': "form-control total_price_excluding_tax"}),

        }
        fields = ['product', 'price', 'quantity', 'tax', 'total_price','total_price_excluding_tax']
