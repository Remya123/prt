from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.utils import timezone
from django.shortcuts import get_object_or_404, render
from django.db import IntegrityError, transaction
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.forms import inlineformset_factory, modelform_factory
from quotation.forms import *
from quotation.models import Quotation


@login_required
@transaction.atomic
def create(request, enquiry_pk=None):
    quotation_form = QuotationForm(request.POST or None)
    QuotationInlineFormSet = inlineformset_factory(
        Quotation, QuotationItem, form=QuotationItemForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    quotation_formset = QuotationInlineFormSet(request.POST or None)

    if request.method == "POST":
        print(quotation_formset.errors)

        if quotation_form.is_valid() and quotation_formset.is_valid():
            quotation = quotation_form.save(commit=False)
            quotation.created_by = request.user.employee
            quotation.code = "QUOTE" + str(Quotation.objects.count() + 1)
            quotation.save()

            quotation_items = quotation_formset.save(commit=False)
            print(quotation_items)
            for quotation_item in quotation_items:
                quotation_item.quotation = quotation
                quotation_item.setTotalPrice()  # set sales order foreign key
                quotation_item.save()

            quotation.setAmount().save()

            # messages.add_message(request, messages.ERROR, "Quotation created successfully")
            return HttpResponseRedirect(reverse('quotation:list'))

    context = {
        'quotationform': quotation_form,
        'quotation_formset': quotation_formset,
        'products': Product.objects.all(),
        'form_url': reverse_lazy('quotation:create'),
        'type': "Create"

    }
    return render(request, "quotation/create.html", context)


@login_required
def index(request):

    quotations = Quotation.objects.all()

    context = {
        'quotations': quotations,

    }

    return render(request, 'quotation/list.html', context)


@login_required
def detail(request, pk):

    quotation = get_object_or_404(Quotation, pk=pk)

    context = {
        'quotation': quotation,

    }

    return render(request, 'quotation/detail.html', context)
