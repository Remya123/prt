from django.db import models
from pms_core.models import ErpAbstractBaseModel
from employee.models import Employee
from django.db import transaction
from customer.models import Customer

from django.core.validators import MinValueValidator
from decimal import Decimal
from product.models import Product
import uuid
from decimal import Decimal


class Quotation(models.Model):

    STATUS_ACCEPTED = 'accepted'
    STATUS_OPEN = 'open'
    STATUS_REJECTED = 'rejected'

    STATUSES = (
        (STATUS_OPEN, "OPEN"),
        (STATUS_ACCEPTED, "ACCEPTED"),
        (STATUS_REJECTED, "REJECTED"),

    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now=True)

    code = models.CharField(max_length=20, unique=True)
    status = models.CharField(max_length=30, choices=STATUSES, default=STATUS_OPEN)
    reason_for_lost = models.TextField(null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=4, default=0, null=True,
                                 blank=True, validators=[MinValueValidator(0)])
    customer = models.ForeignKey(Customer, related_name='quotation')
    created_by = models.ForeignKey(Employee, null=True, related_name='quotation_created_by')
    approved_by = models.ForeignKey(Employee, null=True, blank=True, related_name='quotation_approved_by')
    salesperson = models.ForeignKey(Employee, null=True, blank=True)
    shipping_charge = models.DecimalField(max_digits=20, decimal_places=2, default=0, null=True, blank=True)

    def __str__(self):
        return str(self.code)

    def quotecode(self):
        return self.objects.count() + 1

    def setAmount(self):
        items = self.items

        sub_total = 0
        for item in items.all():
            sub_total = sub_total + item.total_price
        self.amount = sub_total + self.shipping_charge
        return self

    def subTotal(self):
        items = self.items

        sub_total = 0
        for item in items.all():
            sub_total = sub_total + item.total_price
        return sub_total


class QuotationItem(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="quotation_item")
    price = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(0)])
    quantity = models.PositiveIntegerField(default=1)
    tax = models.DecimalField(max_digits=20, decimal_places=4, null=True, blank=True, default=0)
    quotation = models.ForeignKey(Quotation, on_delete=models.CASCADE,
                                  related_name="items", null=True, blank=True)
    total_price = models.DecimalField(max_digits=20, decimal_places=4, blank=True,
                                      null=True, validators=[MinValueValidator(0)])
    total_price_excluding_tax = models.DecimalField(max_digits=20, decimal_places=4, blank=True,
                                                    null=True, validators=[MinValueValidator(0)])

    def __str__(self):
        return str(self.product) + "-" + str(self.quotation)

    def setTotalPrice(self):
        total = 0
        total = (self.price * self.quantity)
        self.total_price_excluding_tax = total
        self.tax = total * Decimal(.05)

        self.total_price = total + self.tax
