from django.conf.urls import include, url

from quotation import views

app_name = "quotation"


urlpatterns = [

    url(r'^quotation/', include([
        url(r'^$', views.index, name="list"),
        url(r'^create/$', views.create, name="create"),
        url(r'^(?P<pk>[^/]+)/detail/$', views.detail, name="detail"),


    ])),

]
