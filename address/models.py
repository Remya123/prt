from django.db import models
# from smart_selects.db_fields import ChainedForeignKey

from pms_core.models import ErpAbstractBaseModel, City


class Address(ErpAbstractBaseModel):

    addressline1 = models.CharField("Addressline 1", max_length=200, null=True, blank=False)
    addressline2 = models.CharField("Addressline 2", max_length=200, null=True, blank=True)
    city = models.ForeignKey(City)


class Contact(ErpAbstractBaseModel):

    phone = models.CharField("Phone Number", max_length=20)
    alternate_phone = models.CharField("Alternate Phone Number", max_length=20, null=True, blank=True)
